FROM debian:10 as stable
RUN apt-get update && apt-get install -y gcc make procps curl nano apt-transport-https ca-certificates gnupg lsb-release && apt-get remove docker docker.io runc
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
RUN echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
RUN apt-get update && apt-get install -y docker-ce docker-ce-cli containerd.io strace
WORKDIR /home
ENTRYPOINT ["top", "-b"]
